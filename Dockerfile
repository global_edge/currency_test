FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7-alpine3.8 as build
WORKDIR /app
COPY . /app
RUN apk add --no-cache --update --virtual .build-deps \
    gcc \
    musl-dev \
    make && \
    pip install pipenv && \
    pipenv install --system --deploy && \
    apk --purge del .build-deps

FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7-alpine3.8
WORKDIR /app
COPY  --from=build /app .
COPY  --from=build /usr/local/lib/python3.7/site-packages/ /usr/local/lib/python3.7/site-packages/
RUN chown -R nobody /app
USER nobody