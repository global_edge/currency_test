"""
Application settings
"""
from os import getenv


class Settings:
    # pylint: disable=too-few-public-methods
    """
    Application settings
    """
    EXCHANGE_RATES_DATA_URI = 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml'
    CURRENCY_RATES_FILENAME = 'currency_rates.xml'
    SECRET_TOKEN = getenv('SECRET_TOKEN')
