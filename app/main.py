# pylint: disable=invalid-name
"""
FastAPI application entrypoint
"""
from fastapi import Depends, FastAPI, Header, HTTPException

from .routers import routes
from .settings import Settings
from .utils import get_currency_rates

app = FastAPI()
settings = Settings()


@app.on_event('startup')
async def startup_event() -> None:
    """
    Fetch currency rates from url during application startup
    Returns:
        None
    """
    await get_currency_rates(settings.EXCHANGE_RATES_DATA_URI, settings.CURRENCY_RATES_FILENAME)


async def get_token_header(x_token: str = Header(...)) -> None:
    """
    Check received token
    Args:
        x_token: secret access token

    Returns:
        Raise exception for bad token or None
    """
    if x_token != settings.SECRET_TOKEN:
        raise HTTPException(status_code=400, detail='X-Token header invalid')


app.include_router(routes.ROUTER, dependencies=[Depends(get_token_header)])
