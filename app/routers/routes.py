"""
API routing
"""
from datetime import date

from fastapi import APIRouter
from pydantic import BaseModel
from starlette.responses import JSONResponse
from starlette.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

from app.utils import get_curr_amount_by_date

ROUTER = APIRouter()


class Response(BaseModel):
    # pylint: disable=too-few-public-methods
    """
    Response model
    """
    amount: float
    currency: str


class ErrMsg(BaseModel):
    # pylint: disable=too-few-public-methods
    """
    Error message model
    """
    error: str


@ROUTER.get(
    '/convert/',
    response_model=Response,
    responses={
        HTTP_400_BAD_REQUEST: {
            'model': ErrMsg,
        }
    },
    tags=['convert'],
    summary='Returns the amount of source currency in the target currency',
    description="To convert please provide "
                "amount: INTEGER, "
                "source and destination ISO currency codes: EUR, USD, etc., "
                "reference date: YYYY-MM-DD"
)
async def convert(amount: float, src_currency: str, dest_currency: str, reference_date: date) -> JSONResponse:
    """
    Route for calculating currency amount
    Returns:
        JSON response
    """
    res_amount, err = await get_curr_amount_by_date(amount, src_currency.upper(), dest_currency.upper(), reference_date)

    if err:
        return JSONResponse(status_code=HTTP_400_BAD_REQUEST, content=err)

    res = {
        'amount': res_amount,
        'currency': dest_currency.upper(),
    }

    return JSONResponse(status_code=HTTP_200_OK, content=res)
