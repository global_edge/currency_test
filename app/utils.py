"""
Utilities for application
"""
import xml.etree.ElementTree as Et  # nosec
from datetime import date
from decimal import Decimal
from typing import Tuple, Dict, Optional

import aiohttp

from app.settings import Settings

SETTINGS = Settings()


async def fetch(session: aiohttp.ClientSession, url: str) -> str:
    """
    Fetch data from url
    Args:
        session: aiohttp ClientSession
        url: url string

    Returns:
        Text from response
    """
    async with session.get(url) as response:
        return await response.text()


async def get_currency_rates(url: str, filename: str) -> None:
    """
    Get currency rates from url
    Args:
        url: url string
        filename: file to write currency rates xml

    Returns:
        None
    """
    async with aiohttp.ClientSession() as session:
        xml = await fetch(session, url)

    with open(filename, 'w+') as f:
        f.write(xml)


async def get_curr_amount_by_date(
        amount: float,
        src_curr: str,
        dest_curr: str,
        ref_date: date
) -> Tuple[Optional[float], Optional[Dict[str, str]]]:
    # pylint: disable=too-many-return-statements
    """
    Get target current amount from source current for required date
    Args:
        amount: amount of source currency
        src_curr: ISO code of source currency
        dest_curr: ISO code of destination currency
        ref_date: reference date of rate

    Returns:
        Amount and ISO code of destination currency and error if occurs
    """
    if amount <= 0:
        return None, {'error': f'amount can be only positive float'}

    tree = Et.parse(SETTINGS.CURRENCY_RATES_FILENAME)
    root = tree.getroot()

    find_str = f'{{http://www.ecb.int/vocabulary/2002-08-01/eurofxref}}Cube[@time="{ref_date}"]/'

    try:
        rates_on_date = root[2].findall(find_str)
    except IndexError:
        return None, {'error': 'inconsistent rates source'}

    if not rates_on_date:
        return None, {'error': f'rates for {ref_date} not found'}

    if src_curr != 'EUR':
        src_curr_rate = await get_curr_rate_by_name(src_curr, rates_on_date)
    else:
        src_curr_rate = 1

    if not src_curr_rate:
        return None, {'error': f'rate for {ref_date} for {src_curr} not found'}

    if dest_curr != 'EUR':
        dest_curr_rate = await get_curr_rate_by_name(dest_curr, rates_on_date)
    else:
        dest_curr_rate = 1

    if not dest_curr_rate:
        return None, {'error': f'rate for {ref_date} for {dest_curr} not found'}

    res = Decimal(amount) / Decimal(src_curr_rate) * Decimal(dest_curr_rate)

    # If result of calculation is out of real value
    if res in (float('inf'), float('-inf')):
        return None, {
            'error': f'calculated results is out of range, '
                     f'source currency rate: {src_curr_rate}, '
                     f'destination currency rate: {dest_curr_rate}'
        }

    return float(res), None


async def get_curr_rate_by_name(curr_name: str, rates_on_date: Et) -> Optional[float]:  # type: ignore
    """
    Get currency rate by ISO code from ElementTree instance
    Args:
        curr_name: currency ISO code
        rates_on_date: ElementTree instance

    Returns:
        currency rate as float or None
    """
    try:
        return float(
            next(
                filter(lambda x: x.attrib.get('currency') == curr_name, rates_on_date)  # type: ignore
            ).attrib.get('rate')
        )
    except (StopIteration, IndexError, ValueError):
        return None
