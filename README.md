How to run the application
===

You can use any OS where git and docker presents. Following steps are for
Ubuntu 18.04 x86_64.

1.Pull code from gitlab
```bash
git clone https://gitlab.com/global_edge/currency_test.git
```

2.Enter project directory
```bash
cd currency_test
```

3.Build docker image
```bash
docker build -t IMAGE_TAG .
```
Tag image with clear name to identify it during next step. For example `currency_test`.

4.Run docker image
```bash
docker run -e SECRET_TOKEN='SECRET_TOKEN_STRING' -d --name CONTAINER_NAME -p LOCAL_PORT:80 IMAGE_TAG
```
Choose clear container name to identify it after deployment. For example `currency_test`.
Choose local port where application will be available. For example `8080`. Use `IMAGE_TAG`
from previous step. After deploying application will be available at
- swagger http://127.0.0.1:8080/docs
- ReDocs http://127.0.0.1:8080/redoc
