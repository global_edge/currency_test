# pylint: disable=missing-docstring
from os import getenv

import pytest
from starlette.testclient import TestClient

from app.main import app

CLIENT = TestClient(app)
SECRET_TOKEN = getenv('SECRET_TOKEN')


@pytest.mark.parametrize(
    'token, amount, src_curr, dest_curr, date, status_code, res_amount',
    [
        (SECRET_TOKEN, 20, 'BRL', 'CAD', '2019-09-13', 200, 6.522106574148206),
        (SECRET_TOKEN, 20, 'Brl', 'CAD', '2019-09-13', 200, 6.522106574148206),
        (SECRET_TOKEN, 20, 'BRL', 'Cad', '2019-09-13', 200, 6.522106574148206),
        (SECRET_TOKEN, 20, 'brl', 'cad', '2019-09-13', 200, 6.522106574148206),
        (SECRET_TOKEN, 20, 'EUR', 'cad', '2019-09-13', 200, 29.326),
        (SECRET_TOKEN, 20, 'brl', 'EUR', '2019-09-13', 200, 4.448002846721821),
    ],
)
def test_convert_ok(token, amount, src_curr, dest_curr, date, status_code, res_amount):
    # pylint: disable=too-many-arguments
    response = CLIENT.get(
        f'/convert/?amount={amount}&src_currency={src_curr}&dest_currency={dest_curr}&reference_date={date}',
        headers={'x-token': token},
    )
    assert response.status_code == status_code
    assert response.json().get('amount') == res_amount
    assert response.json().get('currency') == dest_curr.upper()


@pytest.mark.parametrize(
    'token, amount, src_curr, dest_curr, date, status_code, error_text',
    [
        (SECRET_TOKEN, 20, 'BAD', 'CAD', '2019-09-13', 400, 'rate for 2019-09-13 for BAD not found'),
        (SECRET_TOKEN, 20, 'BRL', 'BAD', '2019-09-13', 400, 'rate for 2019-09-13 for BAD not found'),
        (SECRET_TOKEN, 20, 'BRL', 'CAD', '9999-09-13', 400, 'rates for 9999-09-13 not found'),
        (SECRET_TOKEN, -20, 'BRL', 'CAD', '2019-09-13', 400, 'amount can be only positive float'),
        (SECRET_TOKEN, 0, 'BRL', 'CAD', '2019-09-13', 400, 'amount can be only positive float'),
        (SECRET_TOKEN, 10 ** 321, 'BRL', 'CAD', '2019-09-13', 400, 'calculated results is out of range, source currency'
                                                                   ' rate: 4.4964, destination currency rate: 1.4663'),
        (SECRET_TOKEN, 10 ** -324, 'BRL', 'CAD', '2019-09-13', 400, 'amount can be only positive float'),
    ],
)
def test_convert_not_ok(token, amount, src_curr, dest_curr, date, status_code, error_text):
    # pylint: disable=too-many-arguments
    response = CLIENT.get(
        f'/convert/?amount={amount}&src_currency={src_curr}&dest_currency={dest_curr}&reference_date={date}',
        headers={'x-token': token},
    )

    assert response.status_code == status_code
    assert response.json().get('error') == error_text


def test_convert_bad_token():
    response = CLIENT.get(
        '/convert/?amount=20&src_currency=BRL&dest_currency=CAD&reference_date=2019-09-13',
        headers={'x-token': 'BAD'},
    )

    assert response.status_code == 400
    assert response.json().get('detail') == 'X-Token header invalid'


@pytest.mark.parametrize(
    'token, amount, src_curr, dest_curr, date, status_code, error_text',
    [
        (SECRET_TOKEN, 'BAD', 'BRL', 'CAD', '2019-09-13', 422, 'value is not a valid float'),
        (SECRET_TOKEN, 20, 'BRL', 'CAD', 'BAD', 422, 'invalid date format'),
        (SECRET_TOKEN, None, 'BRL', 'CAD', '2019-09-13', 422, 'value is not a valid float'),
    ],
)
def test_convert_bad_amount(token, amount, src_curr, dest_curr, date, status_code, error_text):
    # pylint: disable=too-many-arguments
    response = CLIENT.get(
        f'/convert/?amount={amount}&src_currency={src_curr}&dest_currency={dest_curr}&reference_date={date}',
        headers={'x-token': token},
    )
    assert response.status_code == status_code
    assert response.json().get('detail')[0].get('msg') == error_text


def test_convert_no_amount():
    response = CLIENT.get(
        '/convert/?src_currency=BRL&dest_currency=CAD&reference_date=2019-09-13',
        headers={'x-token': SECRET_TOKEN},
    )
    assert response.status_code == 422
    assert response.json().get('detail')[0].get('msg') == 'field required'


def test_convert_no_src_curr():
    response = CLIENT.get(
        '/convert/?amount=20&dest_currency=CAD&reference_date=2019-09-13',
        headers={'x-token': SECRET_TOKEN},
    )
    assert response.status_code == 422
    assert response.json().get('detail')[0].get('msg') == 'field required'


def test_convert_no_dest_curr():
    response = CLIENT.get(
        '/convert/?amount=20&src_currency=CAD&reference_date=2019-09-13',
        headers={'x-token': SECRET_TOKEN},
    )
    assert response.status_code == 422
    assert response.json().get('detail')[0].get('msg') == 'field required'


def test_convert_no_date():
    response = CLIENT.get(
        '/convert/?amount=20&src_currency=BRL&&dest_currency=CAD',
        headers={'x-token': SECRET_TOKEN},
    )
    assert response.status_code == 422
    assert response.json().get('detail')[0].get('msg') == 'field required'
